## What does this MR do and why?

__

<!--
Describe in detail what your merge request does and why.

Please keep this description updated with any discussion that takes place so
that reviewers can understand your intent. Keeping the description updated is
especially important if they didn't participate in the discussion.
-->

%{all_commits}

## How to set up and validate locally

_Numbered steps to set up and validate the change are strongly suggested._

<!--
Example below:

1. Use this commit hash of finesse: <EXAMPLE_COMMIT>
2. Run the following python snippet:
    ```python
    from finesse import model

    model = Model()
    model.parse(<katscript>)
    model.run()
    ```
3. Ensure model runs without exceptions
-->

## Examples of different behaviour

_Show differences in numerical output, improved error messages, exceptions no longer occurring etc._

### %{source_branch} behaviour

### %{target_branch} behaviour


## MR acceptance checklist

This checklist encourages us to confirm any changes have been analyzed to reduce risks in quality, performance, reliability, security, and maintainability.

* [ ] Includes tests for added/modified code
* [ ] Added or modified the documentation if relevant
* [ ] Docstrings added/modified for new/modified functions
* [ ] Committer/reviewer has ran the documentation stage when ready to merge

/assign me
