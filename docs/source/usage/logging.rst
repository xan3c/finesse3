.. include:: /defs.hrst
.. _logging:

Viewing logs
============

|Finesse| contains a comprehensive logging system, issuing useful information during the
parsing, building, running and output phases of a KatScript or Python script.

Making log messages visible
---------------------------

There are numerous ways to configure how and where logs are displayed or stored. Some
common examples are described in the following subsections.

Option 1: use 'logging.basicConfig()'
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The :func:`logging.basicConfig` function is a quick way to set the level, stream and
format of the logs emitted by |Finesse|::

    import logging

    # Anything after this call that emits logs will have them printed to the current
    # shell.
    logging.basicConfig(level=logging.INFO)

The above call sets up logging to :data:`sys.stderr`, which usually means that log
messages will appear in the shell or notebook with which you run |Finesse|. It also
explicitly sets the level to :data:`logging.INFO`, which displays messages with *info*
level or higher (info, warning, error, critical).

Option 2: use command line interface option
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The command line interface (CLI) supports changing the log verbosity of executed scripts
via option flags. See the :ref:`CLI docs <cli>` for more information.

Option 3: wrap your code in a 'logs' context
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The context manager :func:`~finesse.utilities.logging.logs` provides a quick way to turn
on verbose logging for a small block of code::

    import logging
    from finesse import Model
    from finesse.utilities import logs

    # This sets up the root logger to write to stderr.
    logging.basicConfig()

    model = Model()

    with logs(logging.getLogger(), level="debug"):
        # Debug messages emitted by the following call get printed to stderr.
        model.parse("laser l1 P=1")

The :func:`~finesse.utilities.logging.logs` function also accepts a ``handler`` argument
which can for example be set to a :class:`.FinesseStreamHandler` which allows filtering
of particular channels::

    import logging
    from finesse import Model
    from finesse.utilities import logs
    from finesse.utilities.logging import FinesseStreamHandler

    # Instead of using logging.basicConfig, we configure a handler ourselves and pass it
    # as an argument to `logs` below.
    handler = FinesseStreamHandler()
    # Exclude logs emitted in the parser channel.
    handler.exclude("finesse.script.parser")

    model = Model()

    with logs(logging.getLogger(), level="debug", handler=handler):
        # Debug messages emitted by the following call get printed to stderr.
        model.parse("laser l1 P=1")

In the code above, :meth:`.FinesseStreamHandler.exclude` is used to exclude the
``finesse.script.parser`` log channel from being displayed. The method also has basic
wildcard support, allowing whole channel groups to be excluded. This method can be
useful to reduce log message "noise" when debugging complicated code blocks.

.. note::

    If you pass a handler to :func:`~finesse.utilities.logging.logs`, it is added to the
    root logger as an *additional* handler for the duration of the context. If you also
    make a call to :func:`logging.basicConfig` earlier in your script, there will be
    *two* handlers printing to :data:`sys.stderr`, resulting in log messages being
    printed twice. To avoid this, avoid using :func:`logging.basicConfig` when the
    ``handler`` argument is specified.
