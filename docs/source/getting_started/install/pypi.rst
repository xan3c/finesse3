.. include:: ../../defs.hrst
.. _installation-pypi:

From PyPI (with e.g. pip)
-------------------------

When installing using `pip` it is recommended to install |Finesse| in a
virtual environment. This ensures that dependencies required for |Finesse| to run do not
conflict with other packages installed on your computer.

.. seealso::

    For more information on virtual environments, see the `Virtual Environments and
    Packages <https://docs.python.org/3/tutorial/venv.html>`__ section of the Python
    documentation.

|Finesse| is available on PyPI with the package name ``finesse``. There are many package
managers available for Python but the most common is `pip
<https://pip.pypa.io/en/stable/installing/>`__. Instructions for setting up a virtual
environment and installing |Finesse| via pip are given for each supported platform
below.

If you are using `Conda` then please follow the Conda installation instructions instead!

Linux / macOS
~~~~~~~~~~~~~

|Finesse| is not a pure python package, it needs to be compiled. For most platforms,
there should be binary wheels available, which avoids compilation during finesse
installation.

To create a virtual environment, open up a terminal and run:

.. code-block:: console

    $ python -m venv finesse

Activate the virtual environment in your current terminal using:

.. code-block:: console

    $ source finesse/bin/activate

You should now see ``(finesse)`` before the prompt symbol in your console. Finally
install |Finesse| using your favourite Python package manager, e.g. pip, with:

.. code-block:: console

    (finesse) $ pip install finesse

You can quickly verify Finesse is installed correctly by running:

.. code-block:: console

    (finesse) $ kat3 --version

If it lists the Finesse version, everything should be set up.

Known issues
------------

If you are using MacOS with Apple Silicon and do not want to use Conda then there is currently (as of Oct 2022) no compiled Scipy wheels.
This means when you install |Finesse| it will try and compile Scipy from source which will give you many
errors. You can install Scipy from the Homebrew package manager (recommended) or (if you understand the consequences)
cherry-pick it from the Anaconda nightly, for the latest Scipy version:
```
pip install --pre -i https://pypi.anaconda.org/scipy-wheels-nightly/simple scipy
```


Windows
~~~~~~~

Installation of |Finesse| on Windows using `pip` is not supported.
We recommend that you install |Finesse| using Conda or directly compile
it yourself from source.


Limitations
~~~~~~~~~~~

The packaged versions of |Finesse| hosted on PyPi(pip) should be compatible with
most systems: Many Linux distros 64-bit and MacOS (Intel and Apple Silicon). As
these packages are built to work on a vast array of different systems you may not
get optimal performance for running complex simulations.

* The PyPi MacOS wheels have OpenMP disabled as it isn't included by default on OSX systems
* Specific CPU optimisations have been disabled so that they run on as many different CPUs as possible

If you want the highest performance you can get in |Finesse| we recommend you install
it from source code, where it can then make system specific optimisations.
