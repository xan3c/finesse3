.. include:: /defs.hrst
.. _devtools:

===============
Developer tools
===============

The |Finesse| repository and optional extra ``lint`` requirements define configurations
for various helpful development tools. These tools help to maintain code quality in a
collaborative environment. This section provides some additional information these tools
and their configuration.

Tool configuration
------------------

Settings for most tools can be found in their own sections in :source:`pyproject.toml
</pyproject.toml>`. One exception is ``flake8`` which doesn't support ``pyproject.toml``
and instead has its configuration in :source:`tox.ini </tox.ini>`.

.. todo:: briefly document the purpose of included development tools
