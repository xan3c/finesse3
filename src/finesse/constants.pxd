cdef extern from "constants.h":
    long double _PI "PI"
    double _C_LIGHT "C_LIGHT"
    double _RAD2DEG "RAD2DEG"
    double _DEG2RAD "DEG2RAD"
    double _H_PLANCK "H_PLANCK"
